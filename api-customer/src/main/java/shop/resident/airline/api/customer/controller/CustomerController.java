package shop.resident.airline.api.customer.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import shop.resident.airline.api.customer.model.CustomerItem;
import shop.resident.airline.api.customer.model.CustomerRequest;
import shop.resident.airline.api.customer.service.CustomerService;
import shop.resident.airline.common.response.model.CommonResult;
import shop.resident.airline.common.response.model.ListResult;
import shop.resident.airline.common.response.service.ResponseService;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {
    private final CustomerService customerService;
    @PostMapping("/data")
    public CommonResult setCustomer(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomer(request);

        return ResponseService.getSuccessResult();
    }
    @GetMapping("/all")
    public ListResult<CustomerItem> getCustomers() {
        return ResponseService.getListResult(customerService.getCustomers(), true);
    }
}
