package shop.resident.airline.api.customer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.resident.airline.api.customer.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
