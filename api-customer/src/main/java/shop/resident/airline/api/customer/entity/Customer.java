package shop.resident.airline.api.customer.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.resident.airline.api.customer.model.CustomerRequest;
import shop.resident.airline.common.enums.CounselingType;
import shop.resident.airline.common.interfaces.CommonModelBuilder;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String customerName;
    @Column(nullable = false, length = 15)
    private String customerPhone;
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private CounselingType counselingType;
    @Column(nullable = false)
    private LocalDate requestDate;
    @Column(nullable = false)
    private LocalTime requestTime;


    private Customer(CustomerBuilder builder) {
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.counselingType = builder.counselingType;
        this.requestDate = builder.requestDate;
        this.requestTime = builder.requestTime;
    }

    public static class CustomerBuilder implements CommonModelBuilder<Customer> {
        private final String customerName;
        private final String customerPhone;
        private final CounselingType counselingType;
        private final LocalDate requestDate;
        private final LocalTime requestTime;

        public CustomerBuilder(CustomerRequest request) {
            this.customerName = request.getCustomerName();
            this.customerPhone = request.getCustomerPhone();
            this.counselingType = request.getCounselingType();
            this.requestDate = request.getRequestDate();
            this.requestTime = LocalTime.of(request.getRequestTimeHour(), request.getRequestTimeMin());
        }

        @Override
        public Customer build() {
            return new Customer(this);
        }
    }

}
