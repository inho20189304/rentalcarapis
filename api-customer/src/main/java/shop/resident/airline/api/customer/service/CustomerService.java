package shop.resident.airline.api.customer.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.resident.airline.api.customer.entity.Customer;
import shop.resident.airline.api.customer.model.CustomerItem;
import shop.resident.airline.api.customer.model.CustomerRequest;
import shop.resident.airline.api.customer.repository.CustomerRepository;
import shop.resident.airline.common.response.model.ListResult;
import shop.resident.airline.common.response.service.ListConvertService;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public void setCustomer(CustomerRequest request) {
        Customer addData = new Customer.CustomerBuilder(request).build();

        customerRepository.save(addData);
    }

    public ListResult<CustomerItem> getCustomers() {
        List<Customer> originList = customerRepository.findAll();

        List<CustomerItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new CustomerItem.CustomerItemBuilder(item).build()));
        return ListConvertService.settingResult(result);
    }
}
