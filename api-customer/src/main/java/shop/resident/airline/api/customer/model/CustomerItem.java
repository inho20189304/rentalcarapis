package shop.resident.airline.api.customer.model;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.resident.airline.api.customer.entity.Customer;
import shop.resident.airline.common.enums.CounselingType;
import shop.resident.airline.common.interfaces.CommonModelBuilder;

import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CustomerItem {
    private String customerName;
    private String customerPhone;
    private CounselingType counselingType;
    private LocalTime requestTime;

    private CustomerItem(CustomerItemBuilder builder) {
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.counselingType = builder.counselingType;
        this.requestTime = builder.requestTime;
    }

    public static class CustomerItemBuilder implements CommonModelBuilder<CustomerItem> {
        private final String customerName;
        private final String customerPhone;
        private final CounselingType counselingType;
        private final LocalTime requestTime;

        public CustomerItemBuilder(Customer customer) {
            this.customerName = customer.getCustomerName();
            this.customerPhone = customer.getCustomerPhone();
            this.counselingType = customer.getCounselingType();
            this.requestTime = customer.getRequestTime();
        }

        @Override
        public CustomerItem build() {
            return new CustomerItem(this);
        }
    }
}
