package shop.resident.airline.api.customer.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import shop.resident.airline.common.enums.CounselingType;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class CustomerRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;
    @NotNull
    @Length(min = 12, max= 15)
    private String customerPhone;
    @NotNull
    private CounselingType counselingType;
    @NotNull
    private LocalDate requestDate;
    @Min(0)
    @Max(23)
    private Integer requestTimeHour;
    @Min(0)
    @Max(59)
    private Integer requestTimeMin;

}
