package shop.resident.airline.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CounselingType {
    RENTAL("렌트"),
    LEASE("리스"),
    UNDETERMINED("미정")
    ;

    private final String CounselingTypeName;
}
