package shop.resident.airline.common.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
